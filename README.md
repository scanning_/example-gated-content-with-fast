# Fast + Netlify Authentication

This is an example of using [Fast Login Widget](https://fast.co) to provide access control for a [Netlify](https://netlify.com) site using [Netlify's JWT based visitor access control](https://docs.netlify.com/visitor-access/role-based-access-control/).

Try it out on your own account via this link:

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/scanning1/example-gated-content-with-fast)

Note: To use this functionality your Netlify account must be on a [Teams Business](https://www.netlify.com/pricing/#teams) plan

## Setting Up

Create a Fast app at https://go.fast.co/, specify https://localhost as the redirect initially.

Then click the deploy button above, you will be asked to fill in the following variables at Netlify:

- **FAST_KEY** this is the key of your Fast App
- **FAST_SECRET** this is the secret of your Fast App
- **JWT_SECRET** the JWT_SECRET set globally for your Netlify team or newly created app -- this can be a long random token you generate yourself

Once the site has been created within Netlify, go to site settings, and enable JWT based visitor access control specifying the JWT secret to be the same as in the step above.

Now rename the site or configure a custom domain, and then go to your Fast App created in the steps above, edit the App and add update the redirect URL to point to your your new site's DNS and function (e.g. https://<site-name>.netlify.com/.netlify/functions/fastauth).

You can access a deployed version at [fast-demo.netlify.com](https://fast-demo.netlify.com/)
