import * as t from 'io-ts'
import { failure } from 'io-ts/lib/PathReporter'
import { pipe } from 'fp-ts/lib/pipeable'
import { fold } from 'fp-ts/lib/Either'
import fetch from 'node-fetch'
import { JWT, JWK } from 'jose'

const EventInputCodec = t.type({
  path: t.string,
  httpMethod: t.union([t.literal('GET'), t.literal('POST')]),
  headers: t.object,
  queryStringParameters: t.type({
    challengeId: t.string,
    custom: t.string,
    identifier: t.string,
    oth: t.string,
  }),
  body: t.string,
  isBase64Encoded: t.boolean,
})

const EventResponseCodec = t.union([
  t.type({
    statusCode: t.union([t.literal(200), t.literal(500), t.literal(401), t.literal(302)]),
    body: t.string,
  }),
  t.partial({ headers: t.object }),
])

const FastResponseCodec = t.type({
  status: t.union([t.literal(200), t.literal(500)]),
  success: t.boolean,
  // eslint-disable-next-line @typescript-eslint/camelcase
  fast_id: t.string,
  // eslint-disable-next-line @typescript-eslint/camelcase
  challenge_ip: t.string,
  // eslint-disable-next-line @typescript-eslint/camelcase
  challenge_ts: t.string,
  custom: t.string,
})

type EventInput = t.TypeOf<typeof EventInputCodec>
type EventResponse = t.TypeOf<typeof EventResponseCodec>

const verifyEndpoint = 'https://api.fast.co/api/verify'
const { FAST_KEY, FAST_SECRET, JWT_SECRET, URL } = process.env

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function decode<T, O, I>(input: any, codec: t.Type<T, O, I>): T | undefined {
  return pipe(
    codec.decode(input),
    fold(
      (errors: t.Errors): T | undefined => {
        const error = failure(errors).join('\n')
        const msg = `validation failed: ${error}`
        console.log(msg)
        return
      },
      (v): T => v,
    ),
  )
}

export const handler = async (event: EventInput): Promise<EventResponse> => {
  if (!JWT_SECRET || !FAST_SECRET || !FAST_KEY) {
    console.log(`environment not configured properly`)
    return {
      statusCode: 500,
      body: 'Internal server error',
    }
  }

  // Call Fast to verify the input params (query)
  const response = await fetch(verifyEndpoint, {
    method: 'POST',
    body: `key=${FAST_KEY}&secret=${FAST_SECRET}&identifier=${event.queryStringParameters.identifier}&challengeId=${event.queryStringParameters.challengeId}&oth=${event.queryStringParameters.oth}`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  })

  if (!response.ok) {
    console.log(`Response from fast was not ok, ${response}`)
    return {
      statusCode: 500,
      body: 'Internal server error',
    }
  }

  const responseJson = await response.json()
  const fastResponse = decode(responseJson, FastResponseCodec)
  if (!fastResponse) {
    console.log(`Response from fast was not decoded properly, ${responseJson}`)
    return {
      statusCode: 500,
      body: 'Internal server error',
    }
  }

  if (!fastResponse.success) {
    console.log(`Response from fast was not successful, ${fastResponse}`)
    return {
      statusCode: 401,
      body: 'Unauthorized',
    }
  }

  // Create the JWT
  const expiresInHours = 1
  const jwt = JWT.sign(
    {
      // eslint-disable-next-line @typescript-eslint/camelcase
      app_metadata: {
        authorization: {
          roles: ['user'],
        },
      },
    },
    JWK.asKey(JWT_SECRET),
    {
      issuer: `${FAST_KEY}-fast-netlify`,
      subject: event.queryStringParameters.identifier,
      audience: URL,
      expiresIn: `${expiresInHours} hours`,
    },
  )

  // Set up expiry
  const now = new Date()
  const expiresIn = new Date()
  expiresIn.setHours(now.getHours() + expiresInHours)

  const cookie = `nf_jwt=${jwt}; Secure; Http-Only; Path=/; Expires=${expiresIn.toUTCString()}`

  // Set-Cookie: nf_jwt in response, should be a 302 redirect to /
  return {
    statusCode: 302,
    headers: {
      Location: '/',
      'Set-Cookie': cookie,
    },
    body: `Login successful. Redirecting.`,
  }
}
